﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationEvents : MonoBehaviour
{
    public delegate void ShortFadeEvent();
    public static event ShortFadeEvent ShortFade;

    private PlayerController pc;
    private Animator _animator;
    public ParticleSystem watergunPS;
    public ParticleSystem extinguisherPS;

    void Start()
    {
        pc = transform.parent.GetComponent<PlayerController>();
        _animator = GetComponent<Animator>();
    }
    private void Update() {
        if (!_animator.GetCurrentAnimatorStateInfo(0).IsTag("ShootWater"))
        {
            stopWatergunPS();
        }
        if (!_animator.GetCurrentAnimatorStateInfo(0).IsTag("ShootExtinguisher"))
        {
            stopExtinguisherPS();
        }
    }

    public void doShortTeleport()
    {
        if (ShortFade != null)
            ShortFade();
    }

    public void blockWalking()
    {
        pc.BlockPlayerMovement();
    }

    public void unblockWalking()
    {
        pc.isWalkingBlocked = false;
    }
    public void startWatergunPS()
    {
        if (!watergunPS.isEmitting || !watergunPS.isPlaying)
        {
            watergunPS.Play();
        }
    }

    public void startExtinguisherPS()
    {
        if (!extinguisherPS.isEmitting || !extinguisherPS.isPlaying)
        {
            extinguisherPS.Play();
        }            
    }

    public void stopWatergunPS()
    {
        if (watergunPS.isPlaying)
            watergunPS.Stop(true,ParticleSystemStopBehavior.StopEmitting);
    }
    
    public void stopExtinguisherPS()
    {
        if (extinguisherPS.isPlaying)
            extinguisherPS.Stop(true,ParticleSystemStopBehavior.StopEmitting);
    }

    public void defeatedAnimationEnded()
    {
        SceneController.ShowRestartMenu();
    }
}
