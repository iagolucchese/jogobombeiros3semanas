﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    public RectTransform MainMenu;
    public RectTransform OptionsMenu;

    private bool inTransition = false;

    public void QuitGame()
    {
        Application.Quit();
    }

    public void GoToLevelOne()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }

    public void GoToOptionsMenu()
    {
        MainMenu.gameObject.SetActive(false);
        OptionsMenu.gameObject.SetActive(true);
    }

    public void GoToMainMenu()
    {
        MainMenu.gameObject.SetActive(true);
        OptionsMenu.gameObject.SetActive(false);
    }
}
