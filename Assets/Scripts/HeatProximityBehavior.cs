﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatProximityBehavior : MonoBehaviour
{
    public AnimationCurve heatByProximityCurve = new AnimationCurve( new Keyframe( 0f, 1f ), new Keyframe( 0.3f, 0.6f ), new Keyframe( 1f, 0f ) );
    public float heatAtPointBlank = 10f;

    private void OnTriggerStay2D(Collider2D other) 
    {
        PlayerController pc = other.GetComponent<PlayerController>();
        if (pc)
        {
            float d = 1/Vector3.Distance(transform.position, other.transform.position);
            pc.heatPerFrame = heatByProximityCurve.Evaluate(d) * heatAtPointBlank;            
        }
    }

    private void OnTriggerExit2D(Collider2D other) 
    {
        PlayerController pc = other.GetComponent<PlayerController>();
        if (pc)
        {
            pc.heatPerFrame = 0;
        }
    }
}
