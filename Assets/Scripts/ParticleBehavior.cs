﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParticleBehavior : MonoBehaviour
{
    public ParticleSystem part;
    public List<ParticleCollisionEvent> collisionEvents;
    public int particleType = 1; //equivalente ao tipo de arma    

    public float damagePerParticle = 1f;

    void Start()
    {
        part = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }

    void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);
        int i = 0;

        while (i < numCollisionEvents)
        {
            if (other.tag == "Fire")
            {
                FireBehavior fb = other.GetComponent<FireBehavior>();
                fb.takeDamage(damagePerParticle);                
            }
            i++;
        }
    }
}