﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    private static PlayerController _player;

    public static List<GameObject> FireArray;
    public static List<GameObject> NPCArray;

    public static int FiresAtSceneStart;
    public static int NPCsAtSceneStart;

    public Text FireExtText;
    public Text NPCRescuedText;

    public static Text FireExtinguishedText;
    public static Text NPCsRescuedText;

    public GameObject HUDCan;
    public GameObject PauseMenuCan;
    public GameObject RestartCan;
    public GameObject VictoryCan;

    public Text TimerText;
    public Text FailedText;
    public Text HintText;

    public Text VictoryNPCText;
    public Text VictoryFireText;
    public Text VictoryTimerText;

    public static GameObject HUDCanvas;
    public static GameObject PauseMenuCanvas;
    public static GameObject RestartCanvas;
    public static GameObject VictoryCanvas;
    public static Text Timer;

    public float TimerStartsAt = 600;
    private float LevelTimer;
    private bool TimerRanOut = false;
    
    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

        FireArray = new List<GameObject>(GameObject.FindGameObjectsWithTag("Fire"));
        NPCArray = new List<GameObject>(GameObject.FindGameObjectsWithTag("NPC"));

        FiresAtSceneStart = FireArray.Count;
        NPCsAtSceneStart = NPCArray.Count;

        FireExtinguishedText = FireExtText;
        NPCsRescuedText = NPCRescuedText;

        NPCsRescuedText.text = "0 / " + NPCsAtSceneStart;
        FireExtinguishedText.text = "0%";

        HUDCanvas = HUDCan;
        PauseMenuCanvas = PauseMenuCan;
        RestartCanvas = RestartCan;
        VictoryCanvas = VictoryCan;
        Timer = TimerText;

        LevelTimer = TimerStartsAt;
    }

    private void Update() 
    {
        VictoryFireText.text = FireExtText.text;
        VictoryNPCText.text = NPCRescuedText.text;
        VictoryTimerText.text = Timer.text;
        if (Time.timeScale <= 0)
            return;
            
        if (Input.GetButtonDown("Pause"))
        {
            if (Time.timeScale > 0)            
                ShowPauseMenu();
            else
                Unpause();
        }
        if (LevelTimer > 0)
        {
            LevelTimer -= Time.deltaTime;
            int min = Mathf.FloorToInt(LevelTimer / 60);
            int sec = Mathf.FloorToInt(LevelTimer % 60);
            Timer.text = min.ToString("00") + ":" + sec.ToString("00");
        }
        else if (!TimerRanOut)
        {
            LevelTimer = 0;
            Timer.text = "00:00";
            Timer.color = Color.red;

            FailedText.text = "Seu tempo acabou";
            HintText.text = "";

            _player.HandlePlayerDeath(); //timer acabou, mata o player, ggwp

            TimerRanOut = true;
        }

        VictoryFireText.text = FireExtText.text;
        VictoryNPCText.text = NPCRescuedText.text;
        VictoryTimerText.text = Timer.text;
    }

    public void RestartLevel()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ShowPauseMenu()
    {
        if (Time.timeScale > 0)
        {
            HUDCanvas.SetActive(false);
            PauseMenuCanvas.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    public void Unpause()
    {
        if (Time.timeScale <= 0)
        {
            HUDCanvas.SetActive(true);
            PauseMenuCanvas.SetActive(false);
            Time.timeScale = 1f;
        }
    }

    public static void ShowRestartMenu()
    {
        if (Time.timeScale > 0)
        {
            HUDCanvas.SetActive(false);
            RestartCanvas.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    public void BackToMenu()
    {
        Time.timeScale = 1f;
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public static int NumberOfFiresOnScene()
    {
        return FireArray.Count;
    }

    public static int NumberOfNPCsOnScene()
    {
        return NPCArray.Count;
    }

    public static void ShowVictoryScreen()
    {
        _player.BlockPlayerMovement();
        VictoryCanvas.SetActive(true);
        HUDCanvas.SetActive(false);

        Time.timeScale = 0f;
    }

    public static void RemoveFire(GameObject fire)
    {
        FireArray.Remove(fire);
        float f = ((((float)FiresAtSceneStart-(float)FireArray.Count)/FiresAtSceneStart)*100);
        FireExtinguishedText.text = Mathf.RoundToInt(f) + "%";
    }

    public static void RemoveNPC(GameObject npc)
    {
        NPCArray.Remove(npc);
        int i = NPCsAtSceneStart-NPCArray.Count;
        NPCsRescuedText.text = i +  " / " + NPCsAtSceneStart;
        if (NPCArray.Count <= 0)
        {
            ShowVictoryScreen();
        }
    }
}
