﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCBehavior : MonoBehaviour
{
    private GameObject _player;
    private bool IsPlayerInside = false;
    private Animator _animator;
    private bool IsSaved = false;

    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (_player && !IsSaved && IsPlayerInside && Input.GetButtonDown("Interact"))
        {
            _animator.Play("Vanish");
            IsSaved = true;
            _player.GetComponent<PlayerController>().PlayerHealsDamage(3);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Player")
        {
            IsPlayerInside = true;
            _player = other.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.tag == "Player"){
            IsPlayerInside = false;
        }
    }

    private void DoDestroy() {
        SceneController.RemoveNPC(this.gameObject);
        Destroy(this.gameObject);
    }
}
