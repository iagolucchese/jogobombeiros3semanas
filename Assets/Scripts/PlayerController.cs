﻿using UnityEngine;
using System.Collections;
using Prime31;
using System;

public class PlayerController : MonoBehaviour
{
	[Header("Movement Settings")]
	public float gravity = -25f;
	public float runSpeed = 4f;
	public float walkSpeed = 2f;
	public float jumpHeight = 3f;
	public float groundDamping = 7f;
	public float inAirDamping = 5f;  // how fast do we change direction? higher means faster
    public float maxTimeHoldingJumpButton = 1f;
	private float timeJumpButtonHeldDown = 0f;

	[Header("Health Settings")]
	public float maxHealth = 3f;
	public float currentHealth;

	[Header("Heat Settings")]
	public float heatPerFrame = 0f;
	public float heatCapacity = 1000f;
	public float heatLostPerFrame = 1f;
	public float heartsLostWhenMaxHeat = 1f; //quanto de vida perder se a barrinha de calor enxer
	//public float gracePeriodAfterDamageInSeconds = 1f;
	public SimpleHealthBar heatBar;
	private float totalHeat = 0f;

	public bool isWalkingBlocked{get;set;}
	public bool isDead{get;set;}

	[HideInInspector]
	private float normalizedHorizontalSpeed = 0;
	private CharacterController2D _controller;
	private Animator _animator;
    //private SpriteRenderer _spriteRenderer;
	private RaycastHit2D _lastControllerColliderHit;
	private Vector3 _velocity;
	private const int NoGun = 0, WaterGun = 1, Extinguisher = 2;
	private int weaponHeld = WaterGun; //cuidar isso

	public delegate void HealthChanged(float ch, float mh);
	public static event HealthChanged HealthChangedEvent;

	void Awake()
	{
		_animator = GetComponentInChildren<Animator>();
		_controller = GetComponent<CharacterController2D>();
        //_spriteRenderer = GetComponentInChildren<SpriteRenderer>();

		// listen to some events for illustration purposes
		_controller.onControllerCollidedEvent += onControllerCollider;
		_controller.onTriggerEnterEvent += onTriggerEnterEvent;
		_controller.onTriggerExitEvent += onTriggerExitEvent;

		currentHealth = maxHealth;		
	}

	/* private void OnEnable() {
		if (HealthChangedEvent != null)
			HealthChangedEvent(currentHealth, maxHealth);
	} */

	#region Event Listeners

	void onControllerCollider( RaycastHit2D hit )
	{
		// bail out on plain old ground hits cause they arent very interesting
		if( hit.normal.y == 1f )
			return;

		// logs any collider hits if uncommented. it gets noisy so it is commented out for the demo
		//Debug.Log( "flags: " + _controller.collisionState + ", hit.normal: " + hit.normal );
	}


	void onTriggerEnterEvent( Collider2D col )
	{
		//Debug.Log( "onTriggerEnterEvent: " + col.gameObject.name );
	}


	void onTriggerExitEvent( Collider2D col )
	{
		//Debug.Log( "onTriggerExitEvent: " + col.gameObject.name );
	}

	#endregion

	private bool checkPlayerAnimationTag(string tag)
	{
		AnimatorStateInfo asi = _animator.GetCurrentAnimatorStateInfo(0);
		return asi.IsTag(tag);
	}

	#region Movement Functions

	private void movePlayerInDirection(int direction)
	{
		normalizedHorizontalSpeed = direction;
		_animator.SetBool("Walking", true);

		 //flipa o player inteiro, se necessario
		if ( (transform.localScale.x < 0f && direction > 0) || (transform.localScale.x > 0f && direction < 0) )
			transform.localScale = new Vector3( -transform.localScale.x, transform.localScale.y, transform.localScale.z );

		//determina qual animacao usar, e.g. se ele esta no ar, nao muda a animacao
		/* if( _controller.isGrounded )
		{
			
			if (checkPlayerAnimationTag("Jump"))
			{
				_animator.Play("Walk");				
			}
			else if (!checkPlayerAnimationTag("Walk"))
			{
				_animator.Play("WalkWindup");
			}
		} */
	}

	private void horizontalMovement()
	{
		//movimento horizontal
		if(!isWalkingBlocked && Input.GetButton("HorizontalRight"))
		{
			movePlayerInDirection(1);           
		}
		else if (!isWalkingBlocked && Input.GetButton("HorizontalLeft"))
		{
			movePlayerInDirection(-1);
		}
		else
		{
			normalizedHorizontalSpeed = 0;
			_animator.SetBool("Walking", false);
			/* if( _controller.isGrounded )
			{
				if (_animator.GetInteger("FiringWeapon")==1)
					_animator.Play("ShootWater");
				else if (_animator.GetInteger("FiringWeapon")==2)
					_animator.Play("ShootExtinguisher");
				else
					_animator.Play("Idle");
			} */
		}
	}
	
	private void verticalMovement() 
	{
		//movimento vertical
		if (_controller.isGrounded) 
		{
			if(Input.GetButtonDown("Jump"))
			{
				_velocity.y = Mathf.Sqrt(jumpHeight * -gravity);
				if (weaponHeld==1)
				{
					_animator.Play("JumpWatergun");
				}
				else if (weaponHeld==2)
				{
					_animator.Play("JumpExtinguisher");
				}
				else 
				{
					_animator.Play("Jump");
				}
			}
			else 
			{
				timeJumpButtonHeldDown = 0;
			}
		}
		else if (Input.GetButton("Jump"))
		{
			if (timeJumpButtonHeldDown <= maxTimeHoldingJumpButton)
			{
				_velocity.y += Mathf.Sqrt(-gravity * jumpHeight * Time.deltaTime);
				timeJumpButtonHeldDown += Time.deltaTime;
			}
		} 
		else 
		{
			timeJumpButtonHeldDown = maxTimeHoldingJumpButton;
		}
	}

	private bool solvePlayerFiring()
	{
		if (isWalkingBlocked)
			return false;
		if (Input.GetButtonUp("FireWeapon"))
		{
			_animator.SetBool("FiringWeapon", false);
			return false;
		}
		else if (Input.GetButtonDown("Interact"))
		{
			return false;
		}
		else if (Input.GetButtonDown("SwapWeapon"))
		{
			weaponHeld = (weaponHeld==1) ? 2 : 1;
			_animator.SetInteger("SelectedWeapon", weaponHeld);
			return false;			
		}
		else if (Input.GetButtonDown("FireWeapon"))
		{
			_animator.SetBool("FiringWeapon", true);
			return false;

		}
		return true;
	}

	private void solvePlayerMovement()
	{			
		_animator.SetBool("Grounded", _controller.isGrounded);

		if (solvePlayerFiring()) 
		{
			horizontalMovement();
			verticalMovement();
		}
		// apply horizontal speed smoothing it. dont really do this with Lerp. Use SmoothDamp or something that provides more control
		var smoothedMovementFactor = _controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
		var actualMoveSpeed = normalizedHorizontalSpeed * ((_animator.GetBool("FiringWeapon")) ?  walkSpeed : runSpeed);
		_velocity.x = Mathf.Lerp( _velocity.x, actualMoveSpeed, Time.deltaTime * smoothedMovementFactor );

		// apply gravity before moving
		_velocity.y += gravity * Time.deltaTime;
		
		if (_controller.isGrounded)
		{
			_animator.SetBool("Falling",false);
			_animator.SetBool("Jumping",false);
			if(Input.GetAxis("Vertical") < -0.1)
			{
				_velocity.y *= 3f;
				_controller.ignoreOneWayPlatformsThisFrame = true;
			}
		}
		else
		{
			if (!_animator.GetBool("Falling") && _velocity.y < 0)
			{
				timeJumpButtonHeldDown = maxTimeHoldingJumpButton; // pra evitar que tu pule depois de começar a cair
				_animator.SetBool("Falling",true);
				_animator.SetBool("Jumping", false);
			}
			
		}	

		// if holding down bump up our movement amount and turn off one way platform detection for a frame.
		// this lets us jump down through one way platforms
		if( _controller.isGrounded && Input.GetAxis("Vertical") < -0.1)
		{
			_velocity.y *= 3f;
			_controller.ignoreOneWayPlatformsThisFrame = true;
		}

		_controller.move( _velocity * Time.deltaTime );
		// grab our current _velocity to use as a base for all calculations
		_velocity = _controller.velocity;
	}
	
	public void DoTeleport(DoorTeleporterBehavior targetDoor)
	{
		transform.position = targetDoor.gameObject.transform.position;
		FadeAnimationEvents.FadeOutCompleted += UnblockPlayerMovement;
	}

	public void BlockPlayerMovement()
	{

		isWalkingBlocked = true; //desbloqueia o PC de se movimentar aqui
		normalizedHorizontalSpeed = 0;
		_velocity.x = 0;
		timeJumpButtonHeldDown = maxTimeHoldingJumpButton;
		//Debug.Log("block walk");
	}

	public void UnblockPlayerMovement()
	{
		isWalkingBlocked = false; //desbloqueia o PC de se movimentar aqui
		FadeAnimationEvents.FadeOutCompleted -= UnblockPlayerMovement;
		//Debug.Log("unblock walk");
	}

	#endregion

	#region Player Health Functions

	public void PlayerTakesDamage(float damage)
	{
		_animator.Play("DamageTaken");
		PlayerHealthChanged(-damage); //transforma o dano em negativo		
		_velocity.x = 0;
	}

	public void PlayerHealsDamage(float heal)
	{
		PlayerHealthChanged(heal); //mantem a cura como positiva
	}

	void PlayerHealthChanged(float healthChange)
	{
		currentHealth += healthChange;
		if (currentHealth > maxHealth)
			currentHealth = maxHealth;
		if (currentHealth <= 0)
		{
			currentHealth = 0;
			HandlePlayerDeath();
		}
		if (HealthChangedEvent != null)
			HealthChangedEvent(currentHealth, maxHealth);
	}

	public void HandlePlayerDeath()
	{
		_velocity.x = 0;
		isDead = true;
		_animator.Play("DamageDefeated");
	}

	

	#endregion

	void Update()
	{
		if (Time.timeScale <= 0) //se jogo pausado, nao faz mais update
			return;
		if (isDead)
		{
			// pro caso do player morrer no ar
			_velocity.y += gravity * Time.deltaTime; 
			_controller.move( _velocity * Time.deltaTime );
			_velocity = _controller.velocity;
			return;
		}
		_animator.SetInteger("SelectedWeapon", weaponHeld); //ficar de olho nisso
        solvePlayerMovement();
		if (heatPerFrame > 0 && !isWalkingBlocked) //se o player nao pode se mexer, ele nao aumenta o calor
		{
			totalHeat += heatPerFrame;
			if (totalHeat >= heatCapacity)
			{
				PlayerTakesDamage(heartsLostWhenMaxHeat);
				totalHeat = 0;
			}
		}
		else
		{
			if (totalHeat <= 0)
				totalHeat = 0;			
			else
				totalHeat -= heatLostPerFrame;
		}
		heatBar.UpdateBar( totalHeat, heatCapacity );
	}
}
