﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuScroller : MonoBehaviour
{
    public float Speed = 5f;

    public SpriteRenderer backR;
    public SpriteRenderer middleR;
    public SpriteRenderer frontR;

    void Update()
    {
        //uma SENHORA gambiarra, mas pelo jeito nao da ruim
        backR.size += new Vector2(Time.deltaTime * Speed, 0f);
        middleR.size += new Vector2(Time.deltaTime * Speed, 0f);
        frontR.size += new Vector2(Time.deltaTime * Speed, 0f);
    }
}
