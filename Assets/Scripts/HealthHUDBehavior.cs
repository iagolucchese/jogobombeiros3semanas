﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthHUDBehavior : MonoBehaviour
{
    public RectTransform healthUnder;
    public RectTransform healthOver;
    public float spriteSizeInPixels = 100f;
    public float animationSpeed = 1f;
    
    public float playerMaxHealth;
    public float playerCurrentHealth;
    private float lerpDerp;
    private float targetWidthOver;
    private float targetWidthUnder;

    private const int maxAmountOfHearts = 15; //literalmente, o maximo de vida que cabe na tela

    public void UpdateHealth(float ch, float mh)
    {
        playerCurrentHealth = ch;
        playerMaxHealth = mh;
        lerpDerp = 0;
    }

    private void Start() 
    {
        playerCurrentHealth = 3f;
        playerMaxHealth = 3f;
        PlayerController.HealthChangedEvent += UpdateHealth;
    }
    
    private void OnDestroy() {
         PlayerController.HealthChangedEvent -= UpdateHealth;
    }

    private void Update() 
    {
        targetWidthOver = playerCurrentHealth*spriteSizeInPixels;
        targetWidthUnder = playerMaxHealth*spriteSizeInPixels;

        var currentWidthOver = healthOver.rect.width;
        var currentWidthUnder = healthUnder.rect.width;
        if (lerpDerp < 1)
        {
            lerpDerp += Time.deltaTime * animationSpeed;
            healthOver.sizeDelta = new Vector2(Mathf.Lerp(currentWidthOver,targetWidthOver,lerpDerp),spriteSizeInPixels);
            healthUnder.sizeDelta = new Vector2(Mathf.Lerp(currentWidthUnder,targetWidthUnder,lerpDerp),spriteSizeInPixels);
        }
        /* if (targetWidthOver > currentWidth)
        {
            healthOver.sizeDelta = new Vector2(currentWidth+(Time.deltaTime * animationSpeed),spriteSizeInPixels);
            
            if (targetWidthOver < currentWidth)
                healthOver.sizeDelta = new Vector2(targetWidthOver,spriteSizeInPixels);
        }
        else if (targetWidthOver < currentWidth)
        {
            healthOver.sizeDelta = new Vector2(currentWidth-(Time.deltaTime * animationSpeed),spriteSizeInPixels);
            if (targetWidthOver > currentWidth)
                healthOver.sizeDelta = new Vector2(targetWidthOver,spriteSizeInPixels);
        } */
        
    }
}
