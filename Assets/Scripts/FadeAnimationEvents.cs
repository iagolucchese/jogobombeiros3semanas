﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class FadeAnimationEvents : MonoBehaviour
{
    public delegate void FadeInEvent();
    public static event FadeInEvent FadeInCompleted;

    public delegate void FadeOutEvent();
    public static event FadeOutEvent FadeOutCompleted;

    private GameObject vcam;

    private void Start() 
    {
        vcam = GameObject.Find("CM vcam1");
        if (!vcam)
            Debug.LogError("vcam is null");
    }

    void animationEventFadein()
    {
        if (FadeInCompleted != null){
            vcam.SetActive(false);
            vcam.SetActive(true);
            FadeInCompleted();
        }            
    }

    void animationEventFadeout()
    {
        if (FadeOutCompleted != null)
        {
            FadeOutCompleted();
        }
    }
}
