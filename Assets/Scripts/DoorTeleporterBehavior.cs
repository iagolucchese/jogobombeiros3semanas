﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(DoorTeleporterBehavior))]

public class DoorTeleporterBehavior : MonoBehaviour
{
    private Animator _animator;
    private GameObject _player;

    public DoorTeleporterBehavior targetDoor;
    public Image fadePanel;
    public bool IsPlayerInside = false;
    public bool longFade = true;

    void Start()
    {
        if (!fadePanel)
        {
            fadePanel = GameObject.FindGameObjectWithTag("FadePanel").GetComponent<Image>();
            _animator = fadePanel.transform.gameObject.GetComponent<Animator>();
        }
        FadeAnimationEvents.FadeInCompleted += TellPlayerToTeleport;
        PlayerAnimationEvents.ShortFade += TellPlayerToTeleport;
    }

    void Update()
    {
        if (_player && IsPlayerInside && Input.GetButtonDown("Interact"))
        {
            var pc = _player.GetComponent<PlayerController>();
            if (pc.isWalkingBlocked)
                return;
            if (longFade)
            {
                _animator.Play("fadeOut");                
            }
            else
            {
                _player.GetComponentInChildren<Animator>().Play("ShortFadeTeleport");
            }
            pc.BlockPlayerMovement();
        }
    }

    void TellPlayerToTeleport()
    {
        if (IsPlayerInside)
        {
            var pc = _player.GetComponent<PlayerController>();
            pc.isWalkingBlocked = true; //bloqueia o PC de se movimentar aqui
            pc.DoTeleport(targetDoor);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Player")
        {
            IsPlayerInside = true;
            _player = other.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.tag == "Player"){
            IsPlayerInside = false;
        }
    }

    private void OnDisable() {
        FadeAnimationEvents.FadeInCompleted -= TellPlayerToTeleport;
        PlayerAnimationEvents.ShortFade -= TellPlayerToTeleport;
    }
}
