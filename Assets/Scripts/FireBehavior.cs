﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBehavior : MonoBehaviour
{
    public float maxHealth = 1000;
    public float currentHealth;
    public ParticleSystem vapor;

    private bool disableVapor = false;
    private bool fireGone = false;

    void Start()
    {
        currentHealth = maxHealth;
        vapor.Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear);
        //vapor = GetComponentInChildren<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentHealth <= 0 && !fireGone)
            DestroyFire();
        disableVapor = true;
    }

    private void DestroyFire()
    {
        fireGone = true;
        var ps = GetComponent<ParticleSystem>();
        ps.Stop(true, ParticleSystemStopBehavior.StopEmitting);
        GetComponent<Rigidbody2D>().simulated = false;
        SceneController.RemoveFire(this.gameObject);
        Destroy(this.gameObject, 30); //TODO
    }

    public void takeDamage(float damage)
    {
        currentHealth -= damage;
        vapor.Play();
        disableVapor = false;
    }

    private void LateUpdate() 
    {
        if (disableVapor)
        {
            vapor.Stop();
            disableVapor = false;
        }
        else
        {
            disableVapor = true;
        }
    }

    /* private void OnDestroy() 
    {
        SceneController.RemoveFire(this.gameObject);
    } */
}
